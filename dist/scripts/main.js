"use strict";

jQuery(function ($) {
  $(document).on("click", 'a[href^="#"]', function (event) {
    event.preventDefault();
    $("html, body").animate({
      scrollTop: $($.attr(this, "href")).offset().top
    }, 500);
  });
  $(".phone").inputmask({
    mask: "+7(999)-999-99-99",
    showMaskOnHover: false
  });
  $(".t-mob").on("click", function (e) {
    e.preventDefault();
    $(".t-header__nav").slideToggle("fast");
    $(".t-header__top").slideToggle("fast");
  });
  new Swiper(".t-products__cards", {
    navigation: {
      nextEl: ".t-products .swiper-button-next",
      prevEl: ".t-products .swiper-button-prev"
    },
    loop: false,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      760: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper(".t-videos__cards", {
    navigation: {
      nextEl: ".t-videos .swiper-button-next",
      prevEl: ".t-videos .swiper-button-prev"
    },
    loop: false,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      760: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper(".t-docs__cards", {
    navigation: {
      nextEl: ".t-docs .swiper-button-next",
      prevEl: ".t-docs .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      760: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 6
      }
    }
  });
  new Swiper(".t-projects__cards", {
    navigation: {
      nextEl: ".t-projects .swiper-button-next",
      prevEl: ".t-projects .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2
      }
    }
  });
  new Swiper(".t-reviews__cards", {
    pagination: {
      el: ".t-reviews .swiper-pagination",
      type: "fraction"
    },
    navigation: {
      nextEl: ".t-reviews .swiper-button-next",
      prevEl: ".t-reviews .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1
  });
});
//# sourceMappingURL=main.js.map